function hexagon(posX, posY, radius) {
  const rotAngle = 360 / 6
  beginShape()
    for (let i = 0; i < 6; i++) {
      const thisVertex = pointOnCircle(posX, posY, radius, rotAngle * i)
      vertex(thisVertex.x, thisVertex.y)
    }
  endShape(CLOSE)
}

function pointOnCircle(posX, posY, radius, angle) {
  const x = posX + radius * cos(angle)
  const y = posY + radius * sin(angle)

  return createVector(x, y)
}

function randomFiftyChance() {
  const rando = random(1)  // between 0 and 1  

  if (rando > 0.5) {  // 50% de chance.
    return true
  } else {
    return false
  }
}

function getRandomFromPalette() {
  const random_color = floor(random(0, PALETTE.length))
  return PALETTE[random_color]
}

function myTriangle (center, radius, direction) {
  if (direction) {
    beginShape();
    vertex(center + radius * cos(0), radius * sin(0));
    vertex(center + radius * cos(120), radius * sin(120));
    vertex(center + radius * cos(240), radius * sin(240));
    endShape(CLOSE); 
  } else {
    beginShape();
    vertex(center + radius * cos(180), radius * sin(180));
    vertex(center + radius * cos(300), radius * sin(300));
    vertex(center + radius * cos(60), radius * sin(60));
    endShape(CLOSE);
  }
}

const layerConstructors = [
  { name: "Outline Shape", weight: 0.3, init: () => new OutlineShape()},
  { name: "Centered Shape", weight: 0.7, init: () => new CenteredShape()},
  { name: "Circles", weight: 0.2, init: () => new Circle()},
  { name: "Simple Lines", weight: 0.6, init: () => new Line()},
  { name: "Dotted Lines", weight: 0.9, init: () => new DottedLines()},
  { name: "Ring of Shapes", weight: 0.8, init: () => new RingOfShape()},
  { name: "Steddped Hexagons", weight: 0.7, init: () => new SteppedHexagon()},
]